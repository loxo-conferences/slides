function start()
{

  Reveal.initialize();

  Reveal.addEventListener( 'title', function() {
    // Called each time the slide with the "title" state is made visible
    var slide = document.getElementsByClassName("present")[0];
    var title = slide.getElementsByClassName("vertical-left")[0].innerHTML;
    var slide_title = document.getElementById("slide_title");
    /*Setting the title*/
    slide_title.innerHTML=title;
    slide_title.style.visibility = "hidden";
    slide_title.style.display = "initial";
    /*Aligning it*/   
    var top = (slide_title.offsetWidth + document.documentElement.clientHeight)/2;
    slide_title.style.top = top + "px";
    
    slide_title.style.visibility = "initial";
  } );
  
  Reveal.addEventListener( 'Notitle', function() {
    // Called each time the slide with the "Notitle" state is made visible
    slide_title = document.getElementById("slide_title");
    /*Setting the title*/
    slide_title.style.display = "none"; 
  } );
  

}