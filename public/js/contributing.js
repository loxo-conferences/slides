function start()
{

  Reveal.initialize();

  var background = document.getElementsByClassName("backgrounds")[0];

  Reveal.addEventListener( 'clean', function()
  {
    /*Here we should be in clean mode, meaning pictures will take the whole page*/
    var header_height = document.getElementById("header-left").offsetHeight + 5;
    var image_height = window.innerHeight - 2*header_height;
    // All the above comments apply to present slide only
    // Find the picture and its source
    var presentSlide = document.getElementsByClassName("stack present")[0];
    var picture =
presentSlide.getElementsByClassName("present")[0].getElementsByTagName("IMG")[0];
    // Make it bigger
    picture.style.height = image_height + "px";
    picture.style.top = header_height + "px";
    picture.style.position = "absolute";
    picture.style.maxHeight = "none";
    picture.style.margin = "0";
    picture.style.padding = "0";
    picture.style.left = "0";
    picture.style.right = "0";
    picture.style.broder = "0";
    // Hide everything else
    //presentSlide.style.display = "none";
    // Copy bullet points to slide notes
    // Create new slide(s) for important bullet points (as URL, mailing
    // addresses...
  });

  Reveal.addEventListener( 'noClean', function()
  {
    /*Here we should be in normal mode, meaning you should see text and picture
      on slides*/
    // Switch to "normal" background
    // Display header and footer
  });

}
